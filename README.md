# Commerce Role Assign

This module allows a commerce store administrator to select a user role, which
will be assigned to users who register an account during or after the commerce
checkout process.

- For a full description of the module, visit the
  [project page](https://www.drupal.org/project/commerce_role_assign).

- To submit bug reports or feature suggestions, or track changes visit the
  [issue queue](https://www.drupal.org/project/issues/commerce_review_receipt).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following modules:

- [Commerce](https://www.drupal.org/project/commerce)

## Installation

Install as you would normally install a contributed Drupal module. Visit
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules)
for further information.

## Configuration

1. Enable the module.
2. Browse to Commerce -> Configuration -> Checkout flows.
3. Edit a checkout flow.
4. Open settings for the **Login or continue as guest** pane.
5. Enable **Allow registration** and select the **Role** to be used.

The selected role will be used as well if
**Guest registration after checkout** pane is enabled.

## Maintainers

- Greg Mack - [gregcube](https://www.drupal.org/u/gregcube)
