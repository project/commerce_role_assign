<?php

namespace Drupal\commerce_role_assign\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_order\Event\OrderAssignEvent;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\user\UserInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class to provide event subscriber of commerce role.
 */
class CommerceRoleAssignEventSubscriber implements EventSubscriberInterface {

  /**
   * Constructor.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.place.post_transition' => ['placePostTransition'],
      'commerce_order.order.assign' => ['orderAssign', -50],
    ];
  }

  /**
   * Assign customer role should it be needed.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function placePostTransition(WorkflowTransitionEvent $event) {
    $entity = $event->getEntity();
    if ($role = $this->getRegistrationRole($entity)) {
      $this->assignRole($entity->getCustomer(), $role);
    }
  }

  /**
   * Assign customer role when order is assigned.
   *
   * @param \Drupal\commerce_order\Event\OrderAssignEvent $event
   *   The registration role.
   */
  public function orderAssign(OrderAssignEvent $event) {
    if ($role = $this->getRegistrationRole($event->getOrder())) {
      $this->assignRole($event->getCustomer(), $role);
    }
  }

  /**
   * Get the configured registration role.
   *
   * @return string
   *   Role ID or NULL if not set.
   */
  protected function getRegistrationRole(OrderInterface $order): ?string {
    $checkout_flow = $this
      ->entityTypeManager
      ->getStorage('commerce_order_type')
      ->load($order->bundle())
      ->getThirdPartySetting('commerce_checkout', 'checkout_flow');

    $config = $this
      ->entityTypeManager
      ->getStorage('commerce_checkout_flow')
      ->load($checkout_flow)
      ->get('configuration');

    return $config['panes']['login']['registration_role'] ?? NULL;
  }

  /**
   * Assign selected user role and customer.
   *
   * @param \Drupal\user\UserInterface $customer
   *   The $customer as use role.
   * @param string $role
   *   Providing $role of user.
   */
  protected function assignRole(UserInterface $customer, string $role) {
    if ($customer->isAuthenticated() && !$customer->hasRole($role)) {
      $customer->addRole($role);
      $customer->save();
    }
  }

}
