<?php

namespace Drupal\commerce_role_assign\Plugin\Commerce\CheckoutPane;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\Login;

/**
 * Overrides the commerce checkout login pane.
 *
 * @see \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\Login.
 */
class CommerceRoleAssignPane extends Login {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['registration_role'] = [
      '#type' => 'select',
      '#title' => $this->t('Role'),
      '#options' => $this->getRoles(),
      '#states' => [
        'visible' => [
          ':input[name="configuration[panes][login][configuration][allow_registration]"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $this->configuration['registration_role'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['registration_role' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    $summary = parent::buildConfigurationSummary();

    if (!empty($this->configuration['allow_registration'])) {
      $summary .= '<br>';
      $summary .= $this->t('Registration role: @role', [
        '@role' => $this->configuration['registration_role'],
      ]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['registration_role'] = $values['registration_role'];
    }
  }

  /**
   * Provide array of user data.
   *
   * @return array
   *   User roles.
   */
  protected function getRoles(): array {
    $roles = [];

    foreach ($this
      ->entityTypeManager
      ->getStorage('user_role')
      ->loadMultiple() as $rid => $role) {
      if (!in_array($rid, ['anonymous', 'authenticated'])) {
        $roles[$rid] = $role->label();
      }
    }

    return $roles;
  }

}
